﻿using Xamarin.Forms;
using Plugin.Settings;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApiSampleApp
{

    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var seconds = TimeSpan.FromSeconds(5);
            Xamarin.Forms.Device.StartTimer(seconds,
                () =>
            {
                CheckConnection();
                return true;
            }
            );

            MainPage = new NavigationPage(new WebApiSampleAppPage());
        }

        private async void CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                await App.Current.MainPage.DisplayAlert("Message", "Not Connected to Internet", null, "OK");

            }
            else
            {
                return;
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
