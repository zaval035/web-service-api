﻿using Plugin.Settings;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace WebApiSampleApp
{
    public class DictionaryApi
    {
        public string type {get; set;}
        public string definition { get; set; }
        public string example { get; set; }
    }

    public partial class WebApiSampleAppPage : ContentPage
    {
        
        
        public WebApiSampleAppPage()
        {
            InitializeComponent();
            if (CrossSettings.Current.Contains("BackgroundColor"))
            {
                var savedBackgroundColor = CrossSettings.Current.GetValueOrDefault("BackgroundColor", "FFFFFF");
                BackgroundColor = Color.FromHex(savedBackgroundColor);
            }
        }

        async void Handle_SearchPage(object sender, System.EventArgs e)
        {

            string searchTextNoformat = searchText.Text;
            if (CrossConnectivity.Current.IsConnected == true  )
            {
                if (!string.IsNullOrWhiteSpace(searchTextNoformat))
                {
                    HttpClient client = new HttpClient();
                    string resultFinal = "";
                    var uri = new Uri(
                        string.Format(
                            $"https://owlbot.info/api/v2/dictionary/" +
                            searchTextNoformat.ToLower()));

                    var request = new HttpRequestMessage();
                    request.Method = HttpMethod.Get;
                    request.RequestUri = uri;
                    request.Headers.Add("Application", "application / json");

                    HttpResponseMessage response = await client.SendAsync(request);
                    List<DictionaryApi> dictionayApiData = null;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        dictionayApiData = JsonConvert.DeserializeObject<List<DictionaryApi>>(content);
                        foreach (DictionaryApi item in dictionayApiData)
                        {
                            resultFinal += $"\n\n Type: {item.type}";
                            resultFinal += $"\n Definition: {item.definition}";
                            resultFinal += $"\n Example: {item.example}";
                        }
                        Result.Text = resultFinal.Substring(0, resultFinal.Length);

                    }
                }
                else
                {
                    await DisplayAlert("Alert", "Empty", "OK");
                }
            }
            else
            {
                await DisplayAlert("Alert", "Not Connected to Internet", "OK");
            }
        }




    }
}
